package ru.t1.dzelenin.tm.repository;

import org.junit.experimental.categories.Category;
import ru.t1.dzelenin.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {
/*
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Test
    public void add() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findAll().get(0));
    }

    @Test
    public void addList() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionRepository.findAll());
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findAll().get(0));
        Assert.assertEquals(USER1.getId(), sessionRepository.findAll().get(0).getUserId());
    }

    @Test
    public void clear() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION_LIST);
        sessionRepository.removeAll();
        final Integer count = 0;
        Assert.assertEquals(count, sessionRepository.getCount());
    }

    @Test
    public void clearByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER1_SESSION2);
        sessionRepository.removeAll(USER1.getId());
        Assert.assertEquals(0, sessionRepository.getCount(USER1.getId()));
    }

    @Test
    public void existsById() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        Assert.assertFalse(sessionRepository.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(sessionRepository.existsById(USER1_SESSION1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        Assert.assertFalse(sessionRepository.existsById(USER1.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(sessionRepository.existsById(USER1.getId(), USER1_SESSION1.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionRepository.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionRepository.findAll(USER1.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        Assert.assertNull(sessionRepository.findOneById(NON_EXISTING_SESSION_ID));
        sessionRepository.add(USER1_SESSION1);
        @Nullable final Session session = sessionRepository.findOneById(USER1_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER1_SESSION1, session);
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findOneById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertNotEquals(USER2_SESSION1, sessionRepository.findOneById(USER1.getId(), USER2_SESSION1.getId()));
    }

    @Test
    public void getCount() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        Integer count = 0;
        Assert.assertEquals(count, sessionRepository.getCount());
        sessionRepository.add(ADMIN1_SESSION1);
        count = 1;
        Assert.assertEquals(count, sessionRepository.getCount());
    }

    @Test
    public void getCountByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        Assert.assertEquals(0, sessionRepository.getCount(ADMIN.getId()));
        sessionRepository.add(ADMIN1_SESSION1);
        sessionRepository.add(USER1_SESSION1);
        Assert.assertEquals(1, sessionRepository.getCount(ADMIN.getId()));
    }

    @Test
    public void remove() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        @Nullable final Session createdSession = sessionRepository.add(ADMIN1_SESSION1);
        @Nullable final Session removedSession = sessionRepository.removeOne(createdSession);
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN1_SESSION1, removedSession);
        Assert.assertNull(sessionRepository.findOneById(ADMIN1_SESSION1.getId()));
    }

    @Test
    public void removeAll() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(SESSION_LIST);
        sessionRepository.removeAll(SESSION_LIST);
        Integer count = 0;
        Assert.assertEquals(count, sessionRepository.getCount());
    }

    @Test
    public void removeByUserId() {
        Assert.assertNull(sessionRepository.removeOne(ADMIN.getId(), null));
        @Nullable final Session createdSession = sessionRepository.add(ADMIN1_SESSION1);
        Assert.assertNull(sessionRepository.removeOne(null, createdSession));
        @Nullable final Session removedSession = sessionRepository.removeOne(ADMIN.getId(), createdSession);
        Assert.assertEquals(ADMIN1_SESSION1, removedSession);
        Assert.assertNull(sessionRepository.findOneById(ADMIN.getId(), ADMIN1_SESSION1.getId()));
    }

    @Test
    public void removeById() {
        Assert.assertNull(sessionRepository.removeOneById(null));
        Assert.assertNull(sessionRepository.removeOneById(NON_EXISTING_SESSION_ID));
        @Nullable final Session createdSession = sessionRepository.add(ADMIN1_SESSION1);
        @Nullable final Session removedSession = sessionRepository.removeOneById(ADMIN1_SESSION1.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN1_SESSION1, removedSession);
        Assert.assertNull(sessionRepository.findOneById(ADMIN1_SESSION1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertNull(sessionRepository.removeOneById(ADMIN.getId(), null));
        Assert.assertNull(sessionRepository.removeOneById(ADMIN.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertNull(sessionRepository.removeOneById(ADMIN.getId(), USER1_SESSION1.getId()));
        @Nullable final Session createdSession = sessionRepository.add(ADMIN1_SESSION1);
        Assert.assertNull(sessionRepository.removeOneById(null, createdSession.getId()));
        @Nullable final Session removedSession = sessionRepository.removeOneById(ADMIN.getId(), createdSession.getId());
        Assert.assertNotNull(removedSession);
        Assert.assertEquals(ADMIN1_SESSION1, removedSession);
        Assert.assertNull(sessionRepository.findOneById(ADMIN.getId(), ADMIN1_SESSION1.getId()));
    }

    @Test
    public void set() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION_LIST);
        sessionRepository.set(USER2_SESSION_LIST);
        Assert.assertEquals(USER2_SESSION_LIST, sessionRepository.findAll());
    }
*/
}
