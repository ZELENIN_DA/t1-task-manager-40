package ru.t1.dzelenin.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.SortSpecification;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import ru.t1.dzelenin.tm.api.repository.ITaskRepository;
import ru.t1.dzelenin.tm.api.service.IConnectionService;
import ru.t1.dzelenin.tm.api.service.ITaskService;
import ru.t1.dzelenin.tm.comporator.CreatedComparator;
import ru.t1.dzelenin.tm.comporator.DateBeginComparator;
import ru.t1.dzelenin.tm.comporator.StatusComparator;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.enumerated.TaskSort;
import ru.t1.dzelenin.tm.exception.field.DescriptionEmptyException;
import ru.t1.dzelenin.tm.exception.field.IdEmptyException;
import ru.t1.dzelenin.tm.exception.field.NameEmptyException;
import ru.t1.dzelenin.tm.exception.field.UserIdEmptyException;
import ru.t1.dzelenin.tm.model.Task;
import ru.t1.dzelenin.tm.model.TaskProvider;

import java.util.*;

import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static org.mybatis.dynamic.sql.SqlBuilder.select;
import static ru.t1.dzelenin.tm.model.TaskProvider.*;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected SortSpecification getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return created;
        else if (comparator == StatusComparator.INSTANCE) return status;
        else if (comparator == DateBeginComparator.INSTANCE) return dateBegin;
        else return name;
    }

    @NotNull
    @Override
    @SneakyThrows
    public void create(@NotNull final String userId, @NotNull final String name) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final TaskSort sort) {
        return null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public void create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        @NotNull Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public Task updateById(@NotNull final String userId,
                           @NotNull final String id,
                           @NotNull final String name,
                           @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusId(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        task.setStatus(status);
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllByProjectId(userId, projectId);
        }
    }

    @Override
    public void add(@NotNull final Task model) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final Task model) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            model.setUserId(userId);
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    public Collection<Task> set(@NotNull final Collection<Task> models) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.clear();
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return models;
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAll(userId);
        }
    }

    @Override
    public @NotNull List<Task> findAll(@NotNull String userId, @NotNull Comparator<Task> comparator) {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final SelectStatementProvider selectStatementProvider =
                    select(id, TaskProvider.userId, name, description, status, created, dateBegin, dateEnd)
                            .from(task)
                            .where(TaskProvider.userId, isEqualTo(userId))
                            .orderBy(getSortType(comparator))
                            .build()
                            .render(RenderingStrategies.MYBATIS3);
            return repository.findAllComparator(selectStatementProvider);
        }
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findOneById(id);
        }
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findOneByIdAndUserId(userId, id);
        }
    }

    @Override
    public boolean existsById(@NotNull String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.existsById(id);
        }
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.existsByIdAndUserId(userId, id);
        }
    }

    @Override
    public void remove(@NotNull Task model) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.remove(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@NotNull String userId, @NotNull Task model) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.remove(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeById(@NotNull String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final Task task;
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneById(id);
            if (task == null) return;
            repository.remove(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final Task task;
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneByIdAndUserId(userId, id);
            if (task == null) return;
            repository.remove(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void update(@NotNull Task model) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.update(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public long getCount() {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.getCount();
        }
    }

    @Override
    public long getCount(@NotNull String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.getCountByUserId(userId);
        }
    }

}

