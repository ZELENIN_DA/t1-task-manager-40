package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseUsername();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseDriver();

}


