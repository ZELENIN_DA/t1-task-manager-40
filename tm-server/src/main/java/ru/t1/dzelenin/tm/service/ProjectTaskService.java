package ru.t1.dzelenin.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.repository.IProjectRepository;
import ru.t1.dzelenin.tm.api.repository.ITaskRepository;
import ru.t1.dzelenin.tm.api.service.IConnectionService;
import ru.t1.dzelenin.tm.api.service.IProjectTaskService;
import ru.t1.dzelenin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dzelenin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dzelenin.tm.exception.field.IdEmptyException;
import ru.t1.dzelenin.tm.exception.field.UserIdEmptyException;
import ru.t1.dzelenin.tm.model.Task;

import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private SqlSession getSession() {
        return connectionService.getSqlSession();
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @NotNull String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) throw new IdEmptyException();
        if (taskId.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProjectId(projectId);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @NotNull String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) throw new IdEmptyException();
        if (taskId.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProjectId(null);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            try {
                taskRepository.removeTasksByProjectId(projectId);
                projectRepository.removeById(projectId);
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }

}