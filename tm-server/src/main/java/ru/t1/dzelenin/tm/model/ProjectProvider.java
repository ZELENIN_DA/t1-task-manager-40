package ru.t1.dzelenin.tm.model;

import org.mybatis.dynamic.sql.SqlColumn;
import ru.t1.dzelenin.tm.enumerated.Status;

import java.util.Date;

public class ProjectProvider {

    public static final ProjectSqlTable project = new ProjectSqlTable();

    public static final SqlColumn<String> id = project.id;

    public static final SqlColumn<String> userId = project.userId;

    public static final SqlColumn<String> name = project.name;

    public static final SqlColumn<String> description = project.description;

    public static final SqlColumn<Status> status = project.status;

    public static final SqlColumn<Date> created = project.created;

    public static final SqlColumn<Date> dateBegin = project.dateBegin;

    public static final SqlColumn<Date> dateEnd = project.dateEnd;

}
