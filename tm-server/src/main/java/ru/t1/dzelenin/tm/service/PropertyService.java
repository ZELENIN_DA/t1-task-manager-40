package ru.t1.dzelenin.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "12345";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "12345678";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "qwer1234";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "100000";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    public static final String DB_USER = "database.username";

    @NotNull
    public static final String DB_USER_DEFAULT = "database.password";

    @NotNull
    public static final String DB_PASSWORD = "tm";

    @NotNull
    public static final String DB_PASSWORD_DEFAULT = "tm";

    @NotNull
    private static final String DB_URL = "database.url";

    @NotNull
    private static final String DB_URL_DEFAULT = "jdbc:postgresql:taskmanager";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    public static final String DB_DRIVER = "database.driver";

    @NotNull
    public static final String DB_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        @NotNull final String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(envKey)) return System.getProperty(envKey);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @NotNull final String value = getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        @NotNull final String value = getStringValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_DEFAULT);
        return Integer.parseInt(value);
    }

    @Override
    public @NotNull String getDatabaseUsername() {
        return getStringValue(DB_USER, DB_USER_DEFAULT);
    }

    @Override
    public @NotNull String getDatabasePassword() {
        return getStringValue(DB_PASSWORD, DB_PASSWORD_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseUrl() {
        return getStringValue(DB_URL, DB_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DB_DRIVER, DB_DRIVER_DEFAULT);
    }

}

