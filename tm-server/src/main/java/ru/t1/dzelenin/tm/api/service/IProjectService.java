package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnerService<Project> {

    @NotNull
    Project changeProjectStatusId(@NotNull String userId, @NotNull String id, @NotNull Status status);

    void create(@Nullable String userId, @NotNull String name, @NotNull String description);

    void create(@NotNull String userId, @NotNull String name);

    void create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @NotNull
    Project updateById(@NotNull String userId,
                       @NotNull String id,
                       @NotNull String name,
                       @NotNull String description);

}



