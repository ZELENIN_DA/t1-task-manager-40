package ru.t1.dzelenin.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.request.domain.DataBackupLoadRequest;
import ru.t1.dzelenin.tm.enumerated.Role;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";

    @Getter
    @NotNull
    private final String description = "Load backup from file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("Load backup");
        getDomainEndpoint().loadDataBackup(new DataBackupLoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}


